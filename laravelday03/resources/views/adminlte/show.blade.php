@extends('master')

@section('content')
    <div class="mt-3 ml-3">
        <h4> {{ $post->title }} </h4>
        <p> {{ $post->question }} </p>
    </div>
@endsection
