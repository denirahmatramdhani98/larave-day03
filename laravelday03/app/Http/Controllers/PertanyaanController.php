<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pages.create');
    }

    public function store(Request $request){
        $request->validate([
            'title'=>'required|unique:questions',
            'question'=>'required',
        ]);

        DB::table('questions')->insert(
            [
                'title'=>$request['title'],
                'question'=>$request['question']
            ]
        );

        return redirect('pertanyaan')->with('success', 'Question successfully added');
    }

    public function index(){
        $posts = DB::table('questions')->get();
        // dd($posts);
        return view('pages.index', compact('posts'));
    }

    public function show($id){
        $post = DB::table('questions')->where('id', $id)->first();
        return view('pages.show', compact('post'));
    }

    public function edit($id){
        $post = DB::table('questions')->where('id', $id)->first();
        return view('pages.edit', compact('post'));
    }

    public function update($id, Request $request){
        $request->validate([
            'title'=>'required|unique:questions',
            'question'=>'required',
        ]);

        $post = DB::table('questions')
                    ->where('id', $id)
                    ->update([
                        'title' => $request['title'],
                        'question'=> $request['question']
                    ]);

        return redirect('/pertanyaan')->with('success', 'Question successfully updated!');
    }

    public function destroy($id){
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Question successfully deleted!');
    }
}
